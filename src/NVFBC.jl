module NVFBC

using CUDA, Images

export grab_frame

const LIB_NVFBC = "libnvidia-fbc.so"

@enum NVFBC_CAPTURE_TYPE begin 
    NVFBC_CAPTURE_TO_SYS      = 0
    NVFBC_CAPTURE_SHARED_CUDA = 1
    NVFBC_CAPTURE_TO_GL       = 3
end
@enum NVFBC_TRACKING_TYPE begin 
    NVFBC_TRACKING_DEFAULT = 0
    NVFBC_TRACKING_OUTPUT  = 1 
    NVFBC_TRACKING_SCREEN  = 2
end
@enum NVFBC_BOOL NVFBC_FALSE = 0 NVFBC_TRUE = 1
@enum NVFBCSTATUS begin
    NVFBC_SUCCESS             = 0
    NVFBC_ERR_API_VERSION     = 1
    NVFBC_ERR_INTERNAL        = 2
    NVFBC_ERR_INVALID_PARAM   = 3
    NVFBC_ERR_INVALID_PTR     = 4
    NVFBC_ERR_INVALID_HANDLE  = 5
    NVFBC_ERR_MAX_CLIENTS     = 6
    NVFBC_ERR_UNSUPPORTED     = 7
    NVFBC_ERR_OUT_OF_MEMORY   = 8
    NVFBC_ERR_BAD_REQUEST     = 9
    NVFBC_ERR_X               = 10
    NVFBC_ERR_GLX             = 11
    NVFBC_ERR_GL              = 12
    NVFBC_ERR_CUDA            = 13
    NVFBC_ERR_ENCODER         = 14
    NVFBC_ERR_CONTEXT         = 15
    NVFBC_ERR_MUST_RECREATE   = 16
    NVFBC_ERR_VULKAN          = 17
end
@enum NVFBC_BUFFER_FORMAT begin
    NVFBC_BUFFER_FORMAT_ARGB    = 0
    NVFBC_BUFFER_FORMAT_RGB     = 1
    NVFBC_BUFFER_FORMAT_NV12    = 2
    NVFBC_BUFFER_FORMAT_YUV444P = 3
    NVFBC_BUFFER_FORMAT_RGBA    = 4
    NVFBC_BUFFER_FORMAT_BGRA    = 5
end
@enum NVFBC_TOCUDA_FLAGS begin
    NVFBC_TOCUDA_GRAB_FLAGS_NOFLAGS                   = 0
    NVFBC_TOCUDA_GRAB_FLAGS_NOWAIT                    = 1 << 0
    NVFBC_TOCUDA_GRAB_FLAGS_FORCE_REFRESH             = 1 << 1
    NVFBC_TOCUDA_GRAB_FLAGS_NOWAIT_IF_NEW_FRAME_READY = 1 << 2
end

const NVFBC_SESSION_HANDLE = UInt64
const CUdeviceptr = UInt64

mutable struct NvfbcSession
    handle::Ref{NVFBC_SESSION_HANDLE}
end

Base.cconvert(::Type{NVFBC_SESSION_HANDLE}, sess::NvfbcSession) = sess.handle[]
Base.cconvert(::Type{Ref{NVFBC_SESSION_HANDLE}}, sess::NvfbcSession) = sess.handle

struct NVFBC_BOX
    x::UInt32
    y::UInt32
    w::UInt32
    h::UInt32
end

struct NVFBC_SIZE
    w::UInt32
    h::UInt32
end

struct NVFBC_CREATE_HANDLE_PARAMS
        dwVersion::UInt32
        privateData::Ref{Cvoid}
        privateDataSize::UInt32
        bExternallyManagedContext::NVFBC_BOOL
        glxCtx::Ref{Cvoid}
        glxFBConfig::Ref{Cvoid}
end

struct NVFBC_CREATE_CAPTURE_SESSION_PARAMS
    dwVersion::UInt32
    eCaptureType::NVFBC_CAPTURE_TYPE
    eTrackingType::NVFBC_TRACKING_TYPE
    dwOutputId::UInt32
    captureBox::NVFBC_BOX
    frameSize::NVFBC_SIZE
    bWithCursor::NVFBC_BOOL
    bDisableAutoModesetRecovery::NVFBC_BOOL
    bRoundFrameSize::NVFBC_BOOL
    dwSamplingRateMs::UInt32
    bPushModel::NVFBC_BOOL
    bAllowDirectCapture::NVFBC_BOOL
end

struct NVFBC_TOCUDA_SETUP_PARAMS
    dwVersion::UInt32
    eBufferFormat::NVFBC_BUFFER_FORMAT
end

struct NVFBC_FRAME_GRAB_INFO
    dwWidth::UInt32
    dwHeight::UInt32
    dwByteSize::UInt32
    dwCurrentFrame::UInt32
    bIsNewFrame::NVFBC_BOOL
    ulTimestampUs::UInt64
    dwMissedFrames::UInt32
    bRequiredPostProcessing::NVFBC_BOOL
    bDirectCapture::NVFBC_BOOL
end

struct NVFBC_TOCUDA_GRAB_FRAME_PARAMS
    dwVersion::UInt32
    dwFlags::UInt32
    pCUDADeviceBuffer::Ref{CUdeviceptr}
    pFrameGrabInfo::Ref{NVFBC_FRAME_GRAB_INFO};
    dwTimeoutMs::UInt32
end

struct NVFBC_DESTROY_CAPTURE_SESSION_PARAMS
    dwVersion::UInt32
end

struct NVFBC_DESTROY_HANDLE_PARAMS
    dwVersion::UInt32
end

const NVFBC_VERSION_MAJOR = 1
const NVFBC_VERSION_MINOR = 7
const NVFBC_VERSION = NVFBC_VERSION_MINOR | (NVFBC_VERSION_MAJOR << 8) # = 263

nvfbc_struct_version(type, ver) = unsafe_trunc(UInt32, sizeof(type) | (ver << 16) | (NVFBC_VERSION << 24))

const NVFBC_CREATE_CAPTURE_SESSION_PARAMS_VER = nvfbc_struct_version(NVFBC_CREATE_CAPTURE_SESSION_PARAMS, 6) # = 117833792
const NVFBC_CREATE_HANDLE_PARAMS_VER = nvfbc_struct_version(NVFBC_CREATE_HANDLE_PARAMS, 2) # = 117571624
const NVFBC_TOCUDA_SETUP_PARAMS_VER = nvfbc_struct_version(NVFBC_TOCUDA_SETUP_PARAMS, 1) # = 117506056
const NVFBC_DESTROY_CAPTURE_SESSION_PARAMS_VER = nvfbc_struct_version(NVFBC_DESTROY_CAPTURE_SESSION_PARAMS, 1)
const NVFBC_DESTROY_HANDLE_PARAMS_VER = nvfbc_struct_version(NVFBC_DESTROY_HANDLE_PARAMS, 1)

nvfbc_create_handle(sess::NvfbcSession, params::Ref{NVFBC_CREATE_HANDLE_PARAMS}) =
    @ccall(LIB_NVFBC.NvFBCCreateHandle(sess::Ref{NVFBC_SESSION_HANDLE}, 
        params::Ref{NVFBC_CREATE_HANDLE_PARAMS})::Cint) |> NVFBCSTATUS

nvfbc_create_capture(sess::NvfbcSession, params::Ref{NVFBC_CREATE_CAPTURE_SESSION_PARAMS}) =
    @ccall(LIB_NVFBC.NvFBCCreateCaptureSession(sess::NVFBC_SESSION_HANDLE, 
        params::Ref{NVFBC_CREATE_CAPTURE_SESSION_PARAMS})::Cint) |> NVFBCSTATUS

nvfbc_destroy_handle(sess::NvfbcSession) =
    @ccall(LIB_NVFBC.NvFBCDestroyHandle(sess::NVFBC_SESSION_HANDLE, 
        NVFBC_DESTROY_HANDLE_PARAMS(NVFBC_DESTROY_HANDLE_PARAMS_VER)
        ::Ref{NVFBC_DESTROY_HANDLE_PARAMS})::Cint) |> NVFBCSTATUS

nvfbc_destroy_capture(sess::NvfbcSession) =
    @ccall(LIB_NVFBC.NvFBCDestroyCaptureSession(sess::NVFBC_SESSION_HANDLE, 
        NVFBC_DESTROY_CAPTURE_SESSION_PARAMS(NVFBC_DESTROY_CAPTURE_SESSION_PARAMS_VER)
        ::Ref{NVFBC_DESTROY_CAPTURE_SESSION_PARAMS})::Cint) |> NVFBCSTATUS

nvfbc_fbc_to_cuda_setup(sess::NvfbcSession, params::Ref{NVFBC_TOCUDA_SETUP_PARAMS}) =
    @ccall(LIB_NVFBC.NvFBCToCudaSetUp(sess::NVFBC_SESSION_HANDLE, 
        params::Ref{NVFBC_TOCUDA_SETUP_PARAMS})::Cint) |> NVFBCSTATUS

nvfbc_fbc_to_cuda_grab_frame(sess::NvfbcSession, params::Ref{NVFBC_TOCUDA_GRAB_FRAME_PARAMS}) =
    @ccall(LIB_NVFBC.NvFBCToCudaGrabFrame(sess::NVFBC_SESSION_HANDLE, 
        params::Ref{NVFBC_TOCUDA_GRAB_FRAME_PARAMS})::Cint) |> NVFBCSTATUS

nvfbc_get_last_error_str(sess::NvfbcSession) = 
    @ccall(LIB_NVFBC.NvFBCGetLastErrorStr(sess::NVFBC_SESSION_HANDLE)::Cstring) |> unsafe_string

function check_nvfbc_status(sess::NvfbcSession, status::NVFBCSTATUS)
    status == NVFBC_SUCCESS || error(nvfbc_get_last_error_str(sess))
    return status
end

macro nvfbc_check(ex)
    @assert ex.head == :call
    @assert length(ex.args) > 1
    return esc(:(check_nvfbc_status($(ex.args[2]), $ex)))
end

function setup_handle(sess::NvfbcSession)
    params = NVFBC_CREATE_HANDLE_PARAMS(
        NVFBC_CREATE_HANDLE_PARAMS_VER, C_NULL,
        0, NVFBC_FALSE, C_NULL, C_NULL
    ) |> Ref
    @nvfbc_check nvfbc_create_handle(sess, params)
end

function setup_capture(sess::NvfbcSession)
    params = NVFBC_CREATE_CAPTURE_SESSION_PARAMS(
        NVFBC_CREATE_CAPTURE_SESSION_PARAMS_VER,
        NVFBC_CAPTURE_SHARED_CUDA, NVFBC_TRACKING_DEFAULT,
        0, NVFBC_BOX(0, 0, 0, 0), NVFBC_SIZE(0, 0), NVFBC_TRUE, 
        NVFBC_FALSE, NVFBC_FALSE, 0, NVFBC_FALSE, NVFBC_FALSE
    ) |> Ref
    @nvfbc_check nvfbc_create_capture(sess, params)
end

function setup_fbc_to_cuda(sess::NvfbcSession)
    params = NVFBC_TOCUDA_SETUP_PARAMS(
        NVFBC_TOCUDA_SETUP_PARAMS_VER,
        NVFBC_BUFFER_FORMAT_RGB
    ) |> Ref
    @nvfbc_check nvfbc_fbc_to_cuda_setup(sess, params)
end

function fbc_to_cuda_frame_grab(sess::NvfbcSession)
    frame_info = NVFBC_FRAME_GRAB_INFO(
        0, 0, 0, 0, NVFBC_FALSE, 0, 0,
        NVFBC_FALSE, NVFBC_FALSE
    ) |> Ref
    cuDevicePtr = Ref{CUdeviceptr}(0)
    params = NVFBC_TOCUDA_GRAB_FRAME_PARAMS(
        NVFBC_TOCUDA_SETUP_PARAMS_VER, UInt32(NVFBC_TOCUDA_GRAB_FLAGS_NOWAIT),
        cuDevicePtr, frame_info, 0
    ) |> Ref
    @nvfbc_check nvfbc_fbc_to_cuda_grab_frame(sess, params)
    arr = unsafe_wrap(
        CuArray, CuPtr{UInt8}(cuDevicePtr[]),
        (3, Int(frame_info[].dwWidth), Int(frame_info[].dwHeight))
    )
    return arr
end

const NVFBC_SESSION = NvfbcSession(Ref{NVFBC_SESSION_HANDLE}(0))

function init_session(sess)
    setup_handle(sess)
    setup_capture(sess)
    setup_fbc_to_cuda(sess)
    finalizer(sess) do x
        @nvfbc_check nvfbc_destroy_capture(x)
        nvfbc_destroy_handle(x) # Always returns "Unable to cleanup NvFBC"
    end
    return sess
end

function cuda_frame_to_image(cu::CuArray{UInt8,3})
    arr = reinterpret.(Images.N0f8, Array(cu))
    image = Images.colorview(Images.RGB, arr)'
    return image
end

grab_frame() = fbc_to_cuda_frame_grab(NVFBC_SESSION)

image() = cuda_frame_to_image(grab_frame())

function __init__()
    CUDA.initialize_context()
    init_session(NVFBC_SESSION)
end

end
