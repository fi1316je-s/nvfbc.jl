using NVFBC
using Test

@testset "NVFBC.jl" begin
    @test length(size(grab_frame())) == 3
end
